#include <iostream>
#include <time.h>
#include <conio.h>

using namespace std;

int main() {
	srand(time(NULL));
	int array[10];

	for (int i = 0; i < 10; i++) {
		array[i] = rand() % 69 + 1;
	}
	cout << "Unsorted Array: " << endl;
	for (int i = 0; i < 10; i++) {
		cout << array[i] << " " << endl;
	}
	int choice = 0;
	cout << "How would you like to sort the Array? (1 for Ascending, 2 for Descending)" << endl;
	cin >> choice;

	if (choice == 1) {
		cout << "\nAscending Order: " << endl;
		for (int i = 0; i < 10 - 1; i++) {
			for (int j = 0; j < 10 - i - 1; j++) {
				if (array[j] > array[j + 1]) {
					int temp;
					temp = array[j];
					array[j] = array[j + 1];
					array[j + 1] = temp;
				}
			}
		}
		for (int i = 0; i < 10; i++) {
			cout << array[i] << " " << endl;
		}
	}

	else if (choice == 2) {
		cout << "\nDescending Order: " << endl;
		for (int i = 0; i < 10 - 1; i++) {
			for (int j = 0; j < 10 - i - 1; j++) {
				if (array[j] < array[j + 1]) {
					int temp;
					temp = array[j];
					array[j] = array[j + 1];
					array[j + 1] = temp;
				}
			}
		}
		for (int i = 0; i < 10; i++) {
			cout << array[i] << " " << endl;
		}
	}
	int search = 0;
	cout << "Which number would you like to search for? : ";
	cin >> search;
	for (int i = 0; i < 10; i++) {
		if (array[i] == search) {
			cout << "The element is found at " << i + 1 << " position.\n";
			break;
		}
		else if (i == 10) {
			cout << "Element not found.\n";
		}
	}
	_getch;
	return 0;
}