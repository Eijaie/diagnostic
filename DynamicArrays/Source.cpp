#include<iostream>
#include<string>
#include<time.h>
#include"UnorderedArray.h"
using namespace std;

int main() {
	srand(time(NULL));
	int size = 0;
	cout << "Insert number of elements in array: ";
	cin >> size;
	UnorderedArray<int> digits(size);

	for (int i = 0; i < size; i++) {
		int random = rand() % 100 + 1;
		digits.push(random);
		cout << i + 1 << ". " << digits[i] << endl;
	}
	int remove = 0;
	cout << "Insert an element to remove: ";
	cin >> remove;
	
	digits.remove(remove - 1);
	for (int i = 0; i < size - 1; i++) {
		cout << i + 1 << ". " << digits[i] << endl;
	}

	int search = 0;
	cout << "Insert an element to search: ";
	cin >> search;

	digits.linearSearch(search);

	if (digits.linearSearch(search) > 0) {
		cout << "\nThe element is at " << digits.linearSearch(search) + 1 << " position.\n";
	}
	else if (digits.linearSearch(search) <= 0) {
		cout << "\nElement not found.\n";	
	}

	system("pause");
	return 0;
}