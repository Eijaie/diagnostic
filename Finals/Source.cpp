#include "Stack.h"
#include "Queue.h"
#include <iostream>
#include <conio.h>
using namespace std;

int main() {
	int size = 0;
	cout << "Input size for arrays: ";
	cin >> size;

	Queue<int> queue(size);
	Stack<int> stack(size);

	while (1) {
		system("cls");
		int choice = 0;
		cout << "What would you like to do?\n[1] - Push an element into arrays\n[2] - Remove the top element of arrays\n[3] - Display elements of arrays\n[4] - End program\n\n";
		cin >> choice;

		if (choice == 1) {
			cout << "\n\nInput element to be pushed to arrays: ";
			cin >> choice;

			queue.push(choice);
			stack.push(choice);
			
			cout << endl  << endl<< choice << " pushed into Stack and Queue arrays";

			_getch();
		}
		else if (choice == 2) {
			cout << "\n\nRemoved..\nTop of Queue: " << queue[queue.top()];
			cout << "\nTop of Stack: " << stack[stack.top()];

			queue.remove(queue.top());
			stack.remove(stack.top());
			_getch();
		}
		else if (choice == 3) {
			cout << "\n\nElements of Arrays: \n";
			cout << "Queue: ";
			for (int i = 0; i < queue.getSize(); i++) {
				cout << queue[i] << "\t";
			}
			cout << "\nStack: ";
			for (int i = stack.getSize() - 1; i >= 0; i--) {
				cout << stack[i] << "\t";
			}
			_getch();
		}
		else if (choice == 4) {
			cout << "\n\n\nExiting program";
			for (int i = 0; i < 3; i++) {
				cout << ".";
				_getch();
			}
			break;
		}
	}


}