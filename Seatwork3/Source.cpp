#include <iostream>
#include <string>
#include <time.h>
#include <conio.h>

using namespace std;

void printDigits(int input, int sum) {
	if (input <= 0) {
		cout << "\b\b\b = " << sum;
		return;
	}
	int add;
	sum = sum + input % 10;
	cout << input % 10 << " + ";
	input = input / 10;
	printDigits(input, sum);
}

void printFibo(int input, int temp, int prev) {
	int next = 0;
	input = input - 1;
	if (input < 0)
		return;
	cout << temp << " ";
	next = temp + prev;
	temp = prev;
	prev = next;
	printFibo(input, temp, prev);
}

bool checkPrime(int input, int i = 2) {
	if (input <= 2)
		return (input == 2) ? true : false;
	if (input % i == 0)
		return false;
	if (i * i > input)
		return true;
	return checkPrime(input, i + 1);
}

int main() {
	while (1) {
		int input, sum = 0;
		system("cls");
		cout << "What would you like to do?\n[1] - Compute sum of digits of a number\n[2] - Print fibonacci numbers\n[3] - Check if input is prime or not\n[4] - Exit program\n";
		cin >> input;

		if (input == 1) {
			cout << "Input number: ";
			cin >> input;
			printDigits(input, sum);
			_getch();
		}
		else if (input == 2) {
			cout << "Insert number to print up to: ";
			cin >> input;
			int temp = 0;
			int prev = 1;
			printFibo(input, temp, prev);
			_getch();
		}
		else if (input == 3) {
			cout << "Input number to check: ";
			cin >> input;
			if (checkPrime(input))
				cout << "Yerp.";
			else
				cout << "Nawp";
			_getch();
		}
		else if (input == 4) {
			system("cls");
			cout << "Exiting program...";
			break;
		}
	}

	return 0;
}