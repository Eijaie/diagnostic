#include <iostream>
#include <string>
#include <time.h>
#include"Ordered.h"
#include"Unordered.h"
#include<conio.h>

using namespace std;
void main()
{
	srand(time(NULL));
	cout << "Enter size of array: ";
	int size;
	cin >> size;

	UnorderedArray<int> unordered(size);
	OrderedArray<int> ordered(size);

	for (int i = 0; i < size; i++)
	{
		int rng = rand() % 101;
		unordered.push(rng);
		ordered.push(rng);
	}

	while (true) {
		int choice = 0;
		cout << "Generated array: " << endl;
		cout << "Unordered: \t";
		for (int i = 0; i < unordered.getSize(); i++)
			cout << unordered[i] << " ";
		cout << "\n\nOrdered: \t";
		for (int i = 0; i < ordered.getSize(); i++)
			cout << ordered[i] << " ";

		cout << "\n\nWhat would you like to do?\n[1] - Search a number\n[2] - Add random number/s\n[3] - Remove an element\n[4] - Exit program\n";
		cin >> choice;

		if (choice == 1) {
			cout << "\nEnter number to search: ";
			int input;
			cin >> input;
			cout << endl;

			cout << "Unordered Array(Linear Search):\n";
			int result = unordered.linearSearch(input);

			if (result >= 0)
				cout << "Element " << input << " was found at index " << result << ".\n";
			else
				cout << "Element " << input << " not found." << endl;


			cout << "\nOrdered Array(Binary Search):\n";
			result = ordered.binarySearch(input);

			if (result >= 0)
				cout << "Element " << input << " was found at index " << result << ".\n";
			else
				cout << "Element " << input << " not found." << endl;

			_getch();
			system("cls");
		}
		else if (choice == 2) {
			cout << "Input size for expansion: ";
			cin >> size;

			for (int i = 0; i < size; i++)
			{
				int rng = rand() % 101;
				unordered.push(rng);
				ordered.push(rng);
			}
			system("cls");
		}
		else if (choice == 3) {
			int input;
			cout << "Input index to be removed: ";
			cin >> input;
			unordered.remove(input);
			ordered.remove(input);
		}
		else if (choice == 4) {
			cout << "Exiting program..\n";
			break;
			_getch();
		}
	}

	system("pause");
}